#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from urllib3.exceptions import InsecureRequestWarning
import sys

from config import *
from lib import *


def sortie():
    print("Syntaxe : ")
    print("python " + __file__ + " argument")
    print("")
    print("Exemple : ")
    print("Win10Adm")
    print("")
    print("Exemples : ")
    print("python " + __file__ + "idCours Win10Adm")
    exit()


# Variable Api Horizon

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

at = hv_connect(username=usernameHVCS, password=pw, url=urlHVCS, domain=domain)

arguments = sys.argv[1:]
if not arguments:
    sortie()
idCoursADE = arguments[0]
nomPoolARechercher = arguments[1]

SupprGrpPools(nomPoolARechercher, at)
PoolsGrpADE(nomPoolARechercher, idCoursADE, at)
sessionActiveCron15(nomPoolARechercher, at)

hv_disconnect(url=urlHVCS, access_token=at)
