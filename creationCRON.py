#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

from config import *
from lib import *


#### main


db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)

dateDebut = datetime.datetime.today().strftime("%Y-%m-%d")
dateFin = datetime.datetime.today().strftime("%Y-%m-%d")

nettoyageCron()

cur = db.cursor()

requete = (
    "Select * from reservationADE where dateDebut LIKE '"
    + dateDebut
    + "%' AND dateFin LIKE '"
    + dateFin
    + "%';"
)

try:
    cur.execute(requete)
except Exception:
    print("Erreur avec la Requete= " + str(requete))
else:
    res = cur.fetchall()

for r in res:
    rnomADE = r[3]
    ridCours = r[0]
    rdateDebut = r[1]
    rdateFin = r[2]

    cur = db.cursor()

    requete = "Select id_pool from ressources where nomADE='" + rnomADE + "';"

    try:
        cur.execute(requete)
    except Exception:
        print("Erreur avec la Requete= " + str(requete))
    else:
        res2 = cur.fetchall()

    ## Cron 0
    doubleCours = False
    for r2 in res:
        r2dateDebut = r2[1]
        if rdateFin == r2dateDebut:
            doubleCours = True
    if doubleCours == False:
        cron0 = open("/etc/cron.d/cron0_" + str(ridCours), "w")
        cron0.write(
            str(rdateFin.minute)
            + " "
            + str(rdateFin.hour)
            + " "
            + str(rdateFin.day)
            + " "
            + str(rdateFin.month)
            + " * root /usr/bin/python3 "
            + pathARRHES
            + "/resaADE_cron0.py "
            + res2[0][0]
        )
        cron0.close()

    ## Cron 5
    cron5 = open("/etc/cron.d/cron5_" + str(ridCours), "w")
    if rdateDebut.minute < 5:
        cron5.write(
            str(60 - 5 + rdateDebut.minute)
            + " "
            + str(rdateDebut.hour - 1)
            + " "
            + str(rdateDebut.day)
            + " "
            + str(rdateDebut.month)
            + " * root /usr/bin/python3 "
            + pathARRHES
            + "/resaADE_cron5.py "
            + res2[0][0]
        )
    else:
        cron5.write(
            str(rdateDebut.minute - 5)
            + " "
            + str(rdateDebut.hour)
            + " "
            + str(rdateDebut.day)
            + " "
            + str(rdateDebut.month)
            + " * root /usr/bin/python3 "
            + pathARRHES
            + "/resaADE_cron5.py "
            + res2[0][0]
        )
    cron5.close()

    ## Cron 15
    cron15 = open("/etc/cron.d/cron15_" + str(ridCours), "w")
    if rdateDebut.minute < 14:
        cron15.write(
            str(60 - 14 + rdateDebut.minute)
            + " "
            + str(int(rdateDebut.hour) - 1)
            + " "
            + str(rdateDebut.day)
            + " "
            + str(rdateDebut.month)
            + " * root /usr/bin/python3 "
            + pathARRHES
            + "/resaADE_cron15.py "
            + str(ridCours)
            + " "
            + res2[0][0]
        )
    else:
        cron15.write(
            str(rdateDebut.minute - 14)
            + " "
            + str(rdateDebut.hour)
            + " "
            + str(rdateDebut.day)
            + " "
            + str(rdateDebut.month)
            + " * root /usr/bin/python3 "
            + pathARRHES
            + "/resaADE_cron15.py "
            + str(ridCours)
            + " "
            + res2[0][0]
        )
    cron15.close()


dbDisconnect(db)
