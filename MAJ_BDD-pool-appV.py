#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from urllib3.exceptions import InsecureRequestWarning

from config import *
from lib import *


# Variable Api Horizon


requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
adCampus = connexionADCampus(hostnameAD, usernameAD, passwordAD)
at = hv_connect(username=usernameHVCS, password=pw, url=urlHVCS, domain=domain)


insertPoolBDD(at, adCampus)
insertAppVolumeBDD(adCampus)

hv_disconnect(url=urlHVCS, access_token=at)
