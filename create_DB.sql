
CREATE DATABASE `cron_horizon` 
-- cron_horizon.ressources definition

CREATE TABLE `ressources` (
  `nomRessources` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_pool` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_appVolume` int(11) DEFAULT NULL,
  `groupeParDefaut` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conventionNommage` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomADE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- cron_horizon.reservationADE definition

CREATE TABLE `reservationADE` (
  `idCours` int(11) NOT NULL,
  `dateDebut` datetime NOT NULL,
  `dateFin` datetime NOT NULL,
  `nomADE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `listeUtilisateurs` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
