# ARRHES (Ade Réservation des Ressources Horizon pour l'Enseignement Supérieur)

## Objectif 
A l'université de caen Normandie, nous avons fait le choix de déployer VmWare Horizon à destination des étudiants pour leur permettre de travailler sur les logiciels métiers en dehors des horaires de cours. De fait, un certain nombre de machines virtuelles sont acesssibles 24 heures sur 24. Pour répondre à la contrainte de licences logicielles limitées en nombre pour certains logiciels, nous avons mis en place un processus de réservation qui assure que les licences seront disponibles pendant un cours planifié. 

## But de l'outil
L'objectif d'ARRHES est de pouvoir réserver un pool de machines virtuelles présenté par VMware Horizon au travers du logiciel d'emploi du temps pédagogique ADE Campus.

Un pool de machines virtuelles est proposé à un groupe d'utilisateur par défaut (par exemple tous les étudiants d'une formation). Lorsqu'une réservation est positionnée sur ce pool, les utilisateurs connectés sont prévenus en amont que leur session sera déconnectée quelques minutes avant le début du cours si et seulement si ces usagers ne sont pas inscrits à ce cours.

ARRHES récupère la liste des personnes concernées par le cours, c'est-à-dire le ou les enseignants ainsi que le groupe d'étudiants.
Pendant toute la durée du cours, aucune autre personne ne peut se connecter à ce pool de machines virtuelles.

Si à la fin du cours, aucun autre enseignement n'a besoin du pool en question, alors le groupe d'utilisateur par défaut est à nouveau autorisé à l'utiliser. Les usagers connectés peuvent toujours utiliser les machines sur lesquelles ils sont connectés.

## Prérequis techniques
- Utiliser VMware Horizon couplé à un Active Directory.
- Utiliser ADE Campus en remontant les étudiants et les enseignants (et non seulement les noms de groupes d'étudiants)
- Créer un équipement dans ADE Campus qui correspondra à un pool de machines virtuelle sur Horizon
- Mettre en place une base de données MySQL / MariaDB
- Disposer d'un serveur sur lequel sur lequel mettre en place des "crons"
- Python 3 et les modules suivants : python-ldap, requests, xmltodict, mysqlclient

## Principe ARRHES

Le principe d'utilisation d'ARRHES se fait de la manière suivantes:


- Réservation sur ADE Campus d'une ressource correspondant à un pool de machines virtuelles VMware Horizon


- Le scripts ade.py (planifié à l'université de Caen Normandie toutes les nuits à 3:30) récupère les informations des réservations de la journée pour les intégrer dans la base de données prévue à cet effet.


- Le script creationCRON.py (planifié à l'université de Caen Normandie toutes les nuits à 4:00) vient créer les 3 tâches cron se basant sur les trois scripts resaADE_cron0, resaADE_cron5 et resaADE_cron15



Le script MAJ_BDD-pool-appv.py (planifié à l'université de Caen Normandie toutes les nuits à 3:00) sert à alimenter la base de données avec les pools de machines virtuelles de VMWARE Horizon et les App Volumes (non utilisé actuellement, mais anticipé pour une possible réservation de ressources App Volumes)


## En détail

### config-samples.py

Exemple de fichier de configuration (à renommer en config.py) contenant toutes les variables nécessaires au bon fonctionnement du script

### create_DB.sql

Fichier de création de la base de données et des tables

### lib.py

Fichier contenant toutes les fonctions nécessaires au bon fonctionnement d'ARRHES

### MAJ_BDD-pool-appv.py

Import des informations des pools Horizon et App Volumes dans la base de données avec les champs nom du pool/app volumes, id du pool/app volumes, le groupe par défaut qui a accès et la convention de nommage des pools et/ou app volumes.

### ade.py

Import des informations des ressources ADE Campus dans la base de données avec les champs id du cours, date de début, date de fin, le nom ADE et la liste des utilisateurs (nom du compte active directory des enseignants et étudiants)

### creationCRON.py

Création des 3 tâches cron, pour chaque réservation ADE Campus du jour, se basant sur les trois scripts resaADE_cron0, resaADE_cron5 et resaADE_cron15

### resaADE_cron0.py

Script effectuant la suppression du groupe et repositionnement du groupe par défaut sur le pool Horizon réservé

### resaADE_cron5.py

Script effectuant la fermeture des sessions actives ne figurant pas dans la liste des utilisateurs enregistrés dans la réservation ADE Campus

### resaADE_cron15.py

Script effectuant l'envoi de message d'alerte pour prévenir qu'un cours va commencer et que l'utilisateur va être déconnecté. Puis positionnement des droits correspondant à la réservation ADE Campus.
