#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
from config import *
from lib import *

## Liste des ressources lue directement dans la base de données
# listeRessources = list()
# listeRessources.append("vdialencon")
# listeRessources.append("vdicaen")
# listeRessources.append("vdicherbourg")
db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)
requeteSelect = "SELECT nomADE FROM cron_horizon.ressources WHERE nomADE is not NULL;"
cur = db.cursor()
try:
    cur.execute(requeteSelect)
except Exception:
    print("Erreur avec la Requete= " + str(requeteSelect))
    resNomADE = None
else:
    resSelect = cur.fetchall()
    try:
        resNomADE = resSelect
    except:
        resNomADE = None
db.close()
listeRessources = list()
for r in resNomADE:
    listeRessources.append(r[0])

debutDate = datetime.datetime.today().strftime("%m/%d/%Y")
# debutDate = "05/23/2022"
finDate = debutDate

## Connexion
sessionId = ade_connect(usernameADE, passwordADE, urlADE)

## Renseigner le projet
requete = (
    urlADE + "sessionId=" + sessionId + "&function=setProject&projectId=" + anneeEnCours
)
projet = returnOrderedDict(requete)

## Récupération des ressources
## ressources = Equipements dans ADE
listIdCours = list()
dictCours = dict()
for ressource in listeRessources:
    # Ressource
    listeCours = recupererRessources(urlADE, sessionId, ressource, debutDate, finDate)
    for c in listeCours:
        dictAttrsCours = dict()

        dictAttrsCours["nomADE"] = ressource

        date = c["date"]
        startHour = c["startHour"]
        endHour = c["endHour"]
        dictAttrsCours["dateDebut"] = (
            date[6:] + "-" + date[3:5] + "-" + date[:2] + " " + startHour + ":00"
        )
        dictAttrsCours["dateFin"] = (
            date[6:] + "-" + date[3:5] + "-" + date[:2] + " " + endHour + ":00"
        )

        idCours = c["idCours"]
        listIdCours.append(idCours)

        ## Récupération du numéro d'étudiants <-- des emails --> des membres "trainee"
        try:
            listeTraineeId = c["listeTraineeId"]
        except:
            listeTraineeId = None
        if listeTraineeId is not None:
            listeStagiaireId = list()
            # listeStagiaireEmail = list()
            listeStagiaireNumeroEtu = list()
            for stagiaireId in listeTraineeId:
                requete = (
                    urlADE
                    + "sessionId="
                    + sessionId
                    + "&function=getResources&tree=false&id="
                    + stagiaireId
                    + "&category=trainee&detail=13"
                )
                data = returnOrderedDict(requete)
                member = data["trainees"]["trainee"]["allMembers"]["member"]
                for r in member:
                    NOMPrenom = r["@name"]
                    idNOMPrenom = r["@id"]
                    listeStagiaireId.append(idNOMPrenom)
            for s in listeStagiaireId:
                requete = (
                    urlADE
                    + "sessionId="
                    + sessionId
                    + "&function=getResources&tree=false&id="
                    + s
                    + "&category=category5&detail=11"
                )
                ## "&category=category5&detail=6" <- détail adapté pour afficher @email
                data = returnOrderedDict(requete)
                # stagiaireEmail = data["categories5"]["category5"]["@email"]
                # listeStagiaireEmail.append(stagiaireEmail)
                stagiaireNumeroEtu = data["categories5"]["category5"]["@code"]
                listeStagiaireNumeroEtu.append(stagiaireNumeroEtu)

        ## Récupération des emails des membres "category5"
        try:
            listeCategory5Id = c["listeCategory5Id"]
        except:
            listeCategory5Id = None
        if listeCategory5Id is not None:
            listeCategory5Code = list()
            for category5Id in listeCategory5Id:
                requete = (
                    urlADE
                    + "sessionId="
                    + sessionId
                    + "&function=getResources&tree=false&id="
                    + category5Id
                    + "&category=category5&detail=11"
                )
                data = returnOrderedDict(requete)
                category5Code = data["categories5"]["category5"]["@code"]
                listeCategory5Code.append(category5Code)

        ## Récupération de l'email de l'instructeur
        try:
            listeInstructorId = c["listeInstructorId"]
        except:
            listeInstructorId = None
        if listeInstructorId is not None:
            # listeInstructeurEmail = list()
            listeInstructeurNumeroOctopus = list()
            for instructeurId in listeInstructorId:
                requete = (
                    urlADE
                    + "sessionId="
                    + sessionId
                    + "&function=getResources&tree=false&id="
                    + instructeurId
                    + "&category=instructor&detail=11"
                )
                ## "&category=instructor&detail=6" <- détail adapté pour afficher @email
                data = returnOrderedDict(requete)
                # instructeurEmail = data["instructors"]["instructor"]["@email"]
                # listeInstructeurEmail.append(instructeurEmail)
                instructeurNumeroOctopus = data["instructors"]["instructor"]["@code"]
                listeInstructeurNumeroOctopus.append(instructeurNumeroOctopus)

        # Récupération du supannAliasLogin de l'utilisateur en fonction de son adresse code (numéro etu ou numéro octopus)
        listSupannAliasLogin = list()
        ldapDSI = connexionLDAP(serveurLDAP, portLDAP, usernameLDAP, passwordLDAP)
        # Etudiants :
        try:
            for s in listeStagiaireNumeroEtu:
                supannAliasLogin = retourneSupannAliasLogin(ldapDSI, baseLDAP, s)
                if supannAliasLogin != None:
                    listSupannAliasLogin.append(supannAliasLogin)
        except:
            # print("il n'y a pas d'étudiant affecté à la ressource")
            pass

        # Enseignants :
        try:
            for i in listeInstructeurNumeroOctopus:
                supannAliasLogin = retourneSupannAliasLogin(ldapDSI, baseLDAP, i)
                if supannAliasLogin != None:
                    listSupannAliasLogin.append(supannAliasLogin)
        except:
            # print("il n'y a pas d'enseignant affecté à la ressource")
            pass

        # Invites :
        try:
            for c in listeCategory5Code:
                supannAliasLogin = retourneSupannAliasLogin(ldapDSI, baseLDAP, c)
                if supannAliasLogin != None:
                    listSupannAliasLogin.append(supannAliasLogin)
        except:
            # print("il n'y a pas d'invité affecté à la ressource")
            pass

        ldapDSI.unbind_s()
        dictAttrsCours["listeUtilisateurs"] = ",".join(listSupannAliasLogin)

        dictCours[idCours] = dictAttrsCours

## Déconnexion
ade_disconnect(urlADE, sessionId)

## Recherche des idCours enregistrés à la date intérogée
db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)
requeteSelect = (
    "SELECT idCours FROM cron_horizon.reservationADE WHERE dateDebut LIKE '"
    + debutDate[6:]
    + "-"
    + debutDate[:2]
    + "-"
    + debutDate[3:5]
    + "%' AND dateFin LIKE '"
    + finDate[6:]
    + "-"
    + finDate[:2]
    + "-"
    + finDate[3:5]
    + "%';"
)
cur = db.cursor()
try:
    cur.execute(requeteSelect)
except Exception:
    print("Erreur avec la Requete= " + str(requeteSelect))
    resIdCours = None
else:
    resSelect = cur.fetchall()
    try:
        resIdCours = resSelect
    except:
        resIdCours = None
db.close()
listeIdCoursSQL = list()
for r in resIdCours:
    listeIdCoursSQL.append(str(r[0]))

db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)

## Suppression des entrées de la base de données MySQL d'un cours qui n'a plus lieu à la date intérogée
for id in listeIdCoursSQL:
    if id not in listIdCours:
        requeteDelete = (
            "DELETE FROM cron_horizon.reservationADE WHERE idCours=" + id + ";"
        )
        print(requeteDelete)
        cur = db.cursor()
        try:
            cur.execute(requeteDelete)
            db.commit()
        except Exception:
            print("Erreur avec la Requete= " + str(requeteDelete))

for id in listIdCours:
    if id in listeIdCoursSQL:
        ## Mise à jour des informations du cours dans la base de données
        requeteUpdate = (
            "UPDATE cron_horizon.reservationADE SET "
            + "dateDebut = '"
            + dictCours[id]["dateDebut"]
            + "', "
            + "dateFin = '"
            + dictCours[id]["dateFin"]
            + "', "
            + "nomADE = '"
            + dictCours[id]["nomADE"]
            + "', "
            + "listeUtilisateurs = '"
            + dictCours[id]["listeUtilisateurs"]
            + "' "
            + "WHERE idCours = "
            + id
            + ";"
        )
        print(requeteUpdate)
        cur = db.cursor()
        try:
            cur.execute(requeteUpdate)
            db.commit()
        except Exception:
            print("Erreur avec la Requete= " + str(requeteUpdate))
    else:
        ## Injection des informations du cours dans la base de données
        requeteInsert = (
            "INSERT INTO cron_horizon.reservationADE (idCours, dateDebut, dateFin, nomADE, listeUtilisateurs) VALUES ("
            + id
            + ", '"
            + dictCours[id]["dateDebut"]
            + "', '"
            + dictCours[id]["dateFin"]
            + "', '"
            + dictCours[id]["nomADE"]
            + "', '"
            + dictCours[id]["listeUtilisateurs"]
            + "');"
        )
        print(requeteInsert)
        cur = db.cursor()
        try:
            cur.execute(requeteInsert)
            db.commit()
        except Exception:
            print("Erreur avec la Requete= " + str(requeteInsert))

db.close()
