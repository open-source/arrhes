#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from ast import arg
# from cmath import polar
# from email.headerregistry import ContentTypeHeader
# from email.mime import application
import re

# from tkinter.messagebox import YES
# from traceback import print_tb
# from regex import P
import requests
import json
from urllib3.exceptions import InsecureRequestWarning
import ldap
import os
import struct
import xmltodict

import MySQLdb
from config import *

# Création d'une connexion vers l'active directory
def connexionADCampus():
    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
    hostname = hostnameAD
    adCampus = ldap.initialize("ldap://" + hostname + ".campus.unicaen.fr:389")
    username = usernameAD
    password = passwordAD
    try:
        r = adCampus.simple_bind(username, password)
        adCampus.set_option(ldap.OPT_REFERRALS, 0)  # nécessaire pour AD...
        try:
            adCampus.result(r)
        except ldap.INVALID_CREDENTIALS:
            print(
                "Connexion … Active Directory ... Les identifiants de connexion doivent être erronés !"
            )
            exit()
    except ldap.SERVER_DOWN:
        print("Connexion … Active Directory ... Le serveur ne répond pas !")
        exit()
    return adCampus


# Création d'une connexion vers le serveur LDAP
def connexionLDAP(serveur, port, username, password):
    """Connexion à l'OpenLDAP"""
    ldapIN = ldap.initialize("ldap://" + serveur + ":" + port)
    try:
        r = ldapIN.simple_bind(username, password)
        try:
            ldapIN.result(r)
        except ldap.INVALID_CREDENTIALS:
            print(
                "Connexion … OpenLDAP ... Les identifiants de connexion doivent être erronés !"
            )
            exit()
    except ldap.SERVER_DOWN:
        print("Connexion … OpenLDAP ... Le serveur ne répond pas !")
        exit()
    return ldapIN


# Recherche un objet LDAP
def rechercheLdap(ldapIN, base, filtre, attribut):
    u = ldapIN.search_s(base, ldap.SCOPE_SUBTREE, filtre, attribut)
    return u


# Récupére les attributs d'un utilisateur
def afficheUtilisateurs(attribut, UsersId):
    adCampus = connexionADCampus()
    membres = ""
    r = rechercheLdap(
        adCampus,
        racineAD,
        "(" + attribut + "=" + UsersId + ")",
        ["objectClass", "member", "displayName", "sAMAccountName"],
    )
    for e in r:
        if str(e[0]) != "None":
            if "group" in str(e[1]["objectClass"]):
                try:
                    membres = e[1]["member"]
                except:
                    pass
            if "user" in str(e[1]["objectClass"]):
                # utilisateur = (
                #     e[1]["displayName"][0].decode("utf-8")
                #     + " ("
                #     + e[1]["sAMAccountName"][0].decode("utf-8")
                #     + ")"
                # )
                utilisateur = e[1]["sAMAccountName"][0].decode("utf-8")
        if membres != "":
            for m in membres:
                m = m.decode("utf-8")
                posVirgule = m.find(",")
                cnUsager = m[3:posVirgule]
                res = rechercheLdap(
                    adCampus,
                    racineAD,
                    "(sAMAccountName=" + cnUsager + ")",
                    ["displayName", "sAMAccountName"],
                )
                for el in res:
                    if str(el[0]) != "None":
                        # utilisateur = (
                        #     el[1]["displayName"][0].decode("utf-8")
                        #     + " ("
                        #     + el[1]["sAMAccountName"][0].decode("utf-8")
                        #     + ")"
                        # )
                        utilisateur = el[1]["sAMAccountName"][0].decode("utf-8")
        adCampus.unbind_s()
        return utilisateur


# Création d'une connexion vers API Horizon
def hv_connect(username, password, domain, url):
    headers = {
        "accept": "*/*",
        "Content-Type": "application/json",
    }

    data = {"domain": domain, "password": password, "username": username}
    json_data = json.dumps(data)

    response = requests.post(
        url + "/rest/login", verify=False, headers=headers, data=json_data
    )
    data = response.json()

    access_token = {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + data["access_token"],
    }
    return access_token


# Déconnexion API Horizon
def hv_disconnect(url, access_token):
    requests.post(url + "/rest/logout", verify=False, headers=access_token)


# Création d'une connexion vers une BDD Mysql
def dbConnect(hostBDD, usernameBDD, passwordBDD, bdd):
    try:
        db = MySQLdb.connect(
            host=hostBDD,
            user=usernameBDD,
            passwd=passwordBDD,
            db=bdd,
        )
    except Exception:
        print("Erreur connexion MySQL")

    return db


# Déconnexion BDD Mysql
def dbDisconnect(db):
    db.close()


# Nettoyage des cron
def nettoyageCron():
    dir = "/etc/cron.d"
    for f in os.listdir(dir):
        if re.search("cron[0,5,15]_*", f):
            os.remove(os.path.join(dir, f))


# Insére dans la BDD les pools Horizons et supprime les pools obsolètes
def insertPoolBDD(at, adCampus):
    db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)
    pools = requests.get(
        urlHVCS + "/rest/inventory/v4/desktop-pools", verify=False, headers=at
    )
    for p in pools.json():
        idPool = p["id"]
        nomPool = p["name"]
        dnamePool = p["display_name"]

        curSelect = db.cursor()
        requeteSelect = (
            "Select id_pool from ressources where id_pool='" + nomPool + "';"
        )
        try:
            curSelect.execute(requeteSelect)
        except Exception:
            print("Erreur avec la Requete= " + str(requeteSelect))
        else:
            resSelect = curSelect.fetchall()

        try:
            grParDefaut = groupADforAppVorPool(dnamePool, adCampus)
        except:
            grParDefaut = None

        if all(item is None for item in resSelect):
            try:
                patternPool = p["pattern_naming_settings"]["naming_pattern"]
                patternPool = patternPool.split("{")
            except:
                patternPool = None

            cur = db.cursor()

            if not patternPool:
                if not grParDefaut:
                    requete = (
                        "INSERT INTO cron_horizon.ressources (nomRessources, id_pool) VALUES ('"
                        + dnamePool
                        + "', '"
                        + nomPool
                        + "');"
                    )
                else:
                    requete = (
                        "INSERT INTO cron_horizon.ressources (nomRessources, id_pool, groupeParDefaut) VALUES ('"
                        + dnamePool
                        + "', '"
                        + nomPool
                        + "', '"
                        + grParDefaut
                        + "');"
                    )

            else:
                if not grParDefaut:
                    requete = (
                        "INSERT INTO cron_horizon.ressources (nomRessources, id_pool, conventionNommage) VALUES ('"
                        + dnamePool
                        + "', '"
                        + nomPool
                        + "', '"
                        + patternPool[0]
                        + "');"
                    )
                else:
                    requete = (
                        "INSERT INTO cron_horizon.ressources (nomRessources, id_pool, conventionNommage, groupeParDefaut) VALUES ('"
                        + dnamePool
                        + "', '"
                        + nomPool
                        + "', '"
                        + patternPool[0]
                        + "', '"
                        + grParDefaut
                        + "');"
                    )

            print(requete)
            try:
                cur.execute(requete)
                db.commit()
            except Exception:
                print("Erreur avec la Requete= " + str(requete))
            else:
                res = cur.fetchall()
                print(res)
        else:
            print("Pool " + resSelect[0][0] + " déjà dans la BDD")

    curSelect = db.cursor()
    requeteSelect = "Select id_pool from ressources where id_pool IS NOT NULL ;"
    try:
        curSelect.execute(requeteSelect)
    except Exception:
        print("Erreur avec la Requete= " + str(requeteSelect))
    else:
        resSelect = curSelect.fetchall()

    for i in resSelect:
        foundU = False
        for p in pools.json():
            nomPool = p["name"]
            if nomPool == i[0]:
                foundU = True
                continue
        if foundU:
            pass
        else:
            cur = db.cursor()

            requete = (
                "DELETE FROM cron_horizon.ressources WHERE id_pool='" + i[0] + "' ;"
            )

            print(requete)
            try:
                cur.execute(requete)
                db.commit()
            except Exception:
                print("Erreur avec la Requete= " + str(requete))
            else:
                res = cur.fetchall()
                print(res)
                print(i[0], "à été supprimé")
    dbDisconnect(db)


# Insére les AppVolumes dans la BDD et supprime les AppV obsolètes
def insertAppVolumeBDD(adCampus):
    ses = requests.Session()
    ses.trust_env = False
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
    db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)
    # Login
    identifiants = {
        "username": loginAppV,
        "password": paswwordAppV,
        "domain": domainAppV,
    }

    res = ses.post(prefixApi + "/sessions", data=identifiants, verify=False)
    # AppStacks
    headers = {"Content-Type": "application/json"}

    # ----- Applications -----

    # ------- SET CURRENT -------
    res = ses.get(
        prefixAV + "/app_products?include=app_markers", headers=headers, verify=False
    )
    dictResAppProduct = json.loads(res.text)
    dictApplications = {}
    for d in dictResAppProduct["data"]:
        nameAppVolume = d["name"]
        idAppVolume = d["id"]
        dictApplications[d["id"]] = d["name"]

        for k, v in dictApplications.items():
            res = ses.get(
                prefixAV
                + "/app_products/"
                + str(k)
                + "/assignments?include=entities,filters",
                headers=headers,
                verify=False,
            )
            valueConvNommage = []
            dictRes = json.loads(res.text)
            for d in dictRes["data"]:
                for p in d["filters"]:
                    valueConvNommage.append(p["value"])

        n = 0
        valueConvNommageN = ""
        while n != len(valueConvNommage):
            valueConvNommageN = valueConvNommageN + " " + valueConvNommage[n]
            n = n + 1

        curSelect = db.cursor()
        requeteSelect = (
            "Select id_pool from ressources where id_appVolume='"
            + str(idAppVolume)
            + "';"
        )
        try:
            curSelect.execute(requeteSelect)
        except Exception:
            print("Erreur avec la Requete= " + str(requeteSelect))
        else:
            resSelect = curSelect.fetchall()

        try:
            grParDefaut = groupADforAppVorPool(nameAppVolume, adCampus)
        except:
            grParDefaut = ""

        if all(item is None for item in resSelect):

            cur = db.cursor()

            requete = (
                "INSERT INTO cron_horizon.ressources (nomRessources, id_appVolume, conventionNommage, groupeParDefaut) VALUES ('"
                + nameAppVolume
                + "', '"
                + str(idAppVolume)
                + "', '"
                + str(valueConvNommageN)
                + "', '"
                + str(grParDefaut)
                + "');"
            )

            print(requete)
            try:
                cur.execute(requete)
                db.commit()
            except Exception:
                print("Erreur avec la Requete= " + str(requete))
            else:
                res = cur.fetchall()
                print(res)
        else:
            print("App Volume " + nameAppVolume + " déjà dans la BDD")

    curSelect = db.cursor()
    requeteSelect = (
        "Select id_appVolume from ressources where id_appVolume IS NOT NULL ;"
    )
    try:
        curSelect.execute(requeteSelect)
    except Exception:
        print("Erreur avec la Requete= " + str(requeteSelect))
    else:
        resSelect = curSelect.fetchall()

    for i in resSelect:
        foundU = False
        for d in dictResAppProduct["data"]:
            idAppVolume = d["id"]
            if idAppVolume == i[0]:
                foundU = True
                continue

        if foundU:
            pass
        else:
            cur = db.cursor()

            requete = (
                "DELETE FROM cron_horizon.ressources WHERE id_appVolume='"
                + str(i[0])
                + "' ;"
            )

            print(requete)
            try:
                cur.execute(requete)
                db.commit()
            except Exception:
                print("Erreur avec la Requete= " + str(requete))
            else:
                res = cur.fetchall()
                print(res)
            print(i[0], "à été supprimé")

    dbDisconnect(db)


# Récupérer la liste des groupes AD pour les Pools horizons et les appVolumes
def groupADforAppVorPool(nomRessources, adCampus):
    groupPool = rechercheLdap(
        adCampus,
        racineGroupeAD,
        "(ObjectClass=group)",
        ["cn", "description"],
    )

    for g in groupPool:
        nomRessourcesDescription = g[1]["description"][0]
        if nomRessources in nomRessourcesDescription.decode("utf-8"):
            cnRessource = g[1]["cn"][0]
            return cnRessource.decode("utf-8")


# Récupérer l'object SID d'un utilisateur active directory
def afficheObjectSID(attribut, UsersId):
    adCampus = connexionADCampus()
    # membres = ""
    r = rechercheLdap(
        adCampus,
        racineAD,
        "(" + attribut + "=" + UsersId + ")",
        ["objectClass", "name", "objectSid"],
    )
    for e in r:
        if str(e[0]) != "None":
            if "group" in str(e[1]["objectClass"]):
                try:
                    membres = e[1]["member"]
                except:
                    pass
            if "group" in str(e[1]["objectClass"]):
                utilisateur = e[1]["objectSid"][0]
        adCampus.unbind_s()
        return utilisateur


# Affecter le groupe par défaut d'un pool Horizon
def PoolsGrpParDefaut(nomPoolARechercher, at):
    db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)

    pools = requests.get(
        urlHVCS + "/rest/inventory/v4/desktop-pools", verify=False, headers=at
    )
    for p in pools.json():
        idPool = p["id"]
        nomPool = p["name"]

        if nomPool == nomPoolARechercher:

            cur = db.cursor()

            requete = (
                "Select groupeParDefaut from ressources where id_pool='"
                + nomPool
                + "';"
            )

            try:
                cur.execute(requete)
                db.commit()
            except Exception:
                print("Erreur avec la Requete= " + str(requete))
            else:
                res = cur.fetchall()

            userObjSID = afficheObjectSID("name", res[0][0])
            userObjSID2 = [convert(userObjSID)]

            data = [
                {
                    "ad_user_or_group_ids": userObjSID2,
                    "id": idPool,
                }
            ]
            json_data = json.dumps(data)

            changeGrpPool = requests.post(
                urlHVCS + "/rest/entitlements/v1/desktop-pools",
                verify=False,
                headers=at,
                data=json_data,
            )

            # print(changeGrpPool)
    dbDisconnect(db)


# Conversion
def convert(binary):
    version = struct.unpack("B", binary[0:1])[0]
    # I do not know how to treat version != 1 (it does not exist yet)
    assert version == 1, version
    length = struct.unpack("B", binary[1:2])[0]
    authority = struct.unpack(b">Q", b"\x00\x00" + binary[2:8])[0]
    string = "S-%d-%d" % (version, authority)
    binary = binary[8:]
    assert len(binary) == 4 * length
    for i in range(length):
        value = struct.unpack("<L", binary[4 * i : 4 * (i + 1)])[0]
        string += "-%d" % value
    return string


# Supprimer les groupes d'un pool Horizon
def SupprGrpPools(nomPoolARechercher, at):
    pools = requests.get(
        urlHVCS + "/rest/inventory/v4/desktop-pools", verify=False, headers=at
    )
    for p in pools.json():
        idPool = p["id"]
        nomPool = p["name"]

        if nomPool == nomPoolARechercher:
            entitlements = requests.get(
                urlHVCS + "/rest/entitlements/v1/desktop-pools",
                verify=False,
                headers=at,
            )
            dictEntitlements = json.loads(entitlements.text)
            for e in dictEntitlements:
                idEntitlements = e["id"]
                if idEntitlements == idPool:
                    entitlementsPool = e["ad_user_or_group_ids"]

                    data = [
                        {
                            "ad_user_or_group_ids": entitlementsPool,
                            "id": idEntitlements,
                        }
                    ]
                    json_data = json.dumps(data)

                    changeGrpPool = requests.delete(
                        urlHVCS + "/rest/entitlements/v1/desktop-pools",
                        verify=False,
                        headers=at,
                        data=json_data,
                    )


# Fermer les sessions des utilisateurs du pool réservé
def sessionActiveCron5(nomPoolARechercher, at):

    session = requests.get(
        urlHVCS + "/rest/inventory/v1/sessions", verify=False, headers=at
    )

    for p in session.json():
        idPoolsession = p["desktop_pool_id"]
        idSession = p["id"]
        idUser = p["user_id"]

        pools = requests.get(
            urlHVCS + "/rest/inventory/v4/desktop-pools", verify=False, headers=at
        )
        for po in pools.json():
            idPool = po["id"]
            nomPool = po["name"]

            if nomPool == nomPoolARechercher:
                if idPool == idPoolsession:
                    entitlements = requests.get(
                        urlHVCS + "/rest/entitlements/v1/desktop-pools",
                        verify=False,
                        headers=at,
                    )
                    dictEntitlements = json.loads(entitlements.text)
                    for e in dictEntitlements:
                        idEntitlements = e["id"]
                        if idEntitlements == idPool:
                            entitlementsPool = e["ad_user_or_group_ids"]
                            if idUser not in entitlementsPool:
                                killSession(idSession, at)


# Tuer une session Horizon
def killSession(dataidSession, at):
    data = [dataidSession]
    json_data = json.dumps(data)
    discoPools = requests.post(
        urlHVCS + "/rest/inventory/v1/sessions/action/disconnect",
        headers=at,
        data=json_data,
    )


# Récupérer les informations utile pour la première cron
def afficheObjectSIDCron15(attribut, UsersId):
    adCampus = connexionADCampus()
    membres = ""
    r = rechercheLdap(
        adCampus,
        racineAD,
        "(" + attribut + "=" + UsersId + ")",
        ["objectClass", "member", "displayName", "sAMAccountName", "objectSid"],
    )
    for e in r:
        if str(e[0]) != "None":
            if "group" in str(e[1]["objectClass"]):
                try:
                    membres = e[1]["member"]
                except:
                    pass
            if "user" in str(e[1]["objectClass"]):
                # utilisateur = (
                #     e[1]["displayName"][0].decode("utf-8")
                #     + " ("
                #     + e[1]["sAMAccountName"][0].decode("utf-8")
                #     + ")"
                # )
                utilisateur = e[1]["objectSid"][0]
        if membres != "":
            for m in membres:
                m = m.decode("utf-8")
                posVirgule = m.find(",")
                cnUsager = m[3:posVirgule]
                res = rechercheLdap(
                    adCampus,
                    racineAD,
                    "(sAMAccountName=" + cnUsager + ")",
                    ["displayName", "sAMAccountName"],
                )
                for el in res:
                    if str(el[0]) != "None":
                        # utilisateur = (
                        #     el[1]["displayName"][0].decode("utf-8")
                        #     + " ("
                        #     + el[1]["sAMAccountName"][0].decode("utf-8")
                        #     + ")"
                        # )
                        print(e[1]["objectSid"][0])
                        utilisateur = el[1]["objectSid"][0]
        adCampus.unbind_s()
        return utilisateur


# Récupérer session active du pool réservé et leur envoyer un message de déconnexion dans 9 min
def sessionActiveCron15(nomPoolARechercher, at):

    session = requests.get(
        urlHVCS + "/rest/inventory/v1/sessions", verify=False, headers=at
    )

    for p in session.json():
        idPoolsession = p["desktop_pool_id"]
        idSession = p["id"]
        idUser = p["user_id"]

        pools = requests.get(
            urlHVCS + "/rest/inventory/v4/desktop-pools", verify=False, headers=at
        )
        for p in pools.json():
            idPool = p["id"]
            nomPool = p["name"]
            if nomPool == nomPoolARechercher:
                if idPool == idPoolsession:
                    entitlements = requests.get(
                        urlHVCS + "/rest/entitlements/v1/desktop-pools",
                        verify=False,
                        headers=at,
                    )
                    dictEntitlements = json.loads(entitlements.text)
                    for e in dictEntitlements:
                        idEntitlements = e["id"]
                        if idEntitlements == idPool:
                            entitlementsPool = e["ad_user_or_group_ids"]
                            if idUser not in entitlementsPool:
                                sendMessage(idSession, at)


# Envoie de message session Horizon
def sendMessage(idSession, at):
    dataidSession = [idSession]
    data = {
        "message": "Votre session va se terminer dans 9 min suite à la réservation d'un cours",
        "message_type": "INFO",
        "session_ids": dataidSession,
    }
    json_data = json.dumps(data)
    messagePools = requests.post(
        urlHVCS + "/rest/inventory/v1/sessions/action/send-message",
        headers=at,
        data=json_data,
    )
    return dataidSession


# Modification droit d'un pool Horizon en fonction d'une réservation ADE
def PoolsGrpADE(nomPoolARechercher, idCoursADE, at):
    db = dbConnect(hostBDD, usernameBDD, passwordBDD, bdd)

    pools = requests.get(
        urlHVCS + "/rest/inventory/v4/desktop-pools", verify=False, headers=at
    )
    for p in pools.json():
        idPool = p["id"]
        nomPool = p["name"]

        if nomPool == nomPoolARechercher:

            cur = db.cursor()

            requete = (
                "Select listeUtilisateurs from reservationADE where idCours='"
                + idCoursADE
                + "';"
            )

            try:
                cur.execute(requete)
                db.commit()
            except Exception:
                print("Erreur avec la Requete= " + str(requete))
            else:
                res = cur.fetchall()

            addUserToGrp = res[0][0].split(",")
            userObjSID = []
            userObjSID2 = []
            for a in addUserToGrp:
                userObjSID.append(afficheObjectSIDCron15("sAMAccountName", a))
            for o in userObjSID:
                userObjSID2.append(convert(o))

            data = [
                {
                    "ad_user_or_group_ids": userObjSID2,
                    "id": idPool,
                }
            ]
            json_data = json.dumps(data)

            changeGrpPool = requests.post(
                urlHVCS + "/rest/entitlements/v1/desktop-pools",
                verify=False,
                headers=at,
                data=json_data,
            )

    dbDisconnect(db)


## Fonction script ade.py
# Récuperer SupannAliasLogin de l'annuaire LDAP
def retourneSupannAliasLogin(ldapIN, baseLDAP, code):
    u = ldapIN.search_s(
        baseLDAP,
        ldap.SCOPE_SUBTREE,
        "(&(supannEtuId=" + code + "))",
        ["supannAliasLogin"],
    )
    try:
        supannAliasLogin = u[0][1]["supannAliasLogin"][0].decode("utf-8")
    except:
        u = ldapIN.search_s(
            baseLDAP,
            ldap.SCOPE_SUBTREE,
            "(&(unicaenRefId=" + code + "))",
            ["supannAliasLogin"],
        )
        try:
            supannAliasLogin = u[0][1]["supannAliasLogin"][0].decode("utf-8")
        except:
            supannAliasLogin = None
    return supannAliasLogin


# Création d'une connexion vers ADE
def ade_connect(username, password, url):
    requete = url + "function=connect&login=" + username + "&password=" + password
    orderedDict = returnOrderedDict(requete)
    # response = requests.get(
    #     url + "function=connect&login=" + username + "&password=" + password
    # )
    # string_xml = response.text
    # orderedDict = xmltodict.parse(string_xml)
    try:
        sessionId = orderedDict["session"]["@id"]
    except:
        print("l'id de session n'a pas été trouvé")
        exit()
    return sessionId


# Déconnexion ADE
def ade_disconnect(url, sessionId):
    requests.get(url + "sessionId=" + sessionId + "&function=disconnect")


def returnOrderedDict(requete):
    response = requests.get(requete)
    string_xml = response.text
    orderedDict = xmltodict.parse(string_xml)
    return orderedDict


# Récupérer les ressources ADE
def recupererRessources(url, sessionId, ressource, dateDebut, dateFin):
    listeCours = list()
    ## Récupération des ressources
    requete = (
        url
        + "sessionId="
        + sessionId
        + "&function=getResources&tree=false&code="
        + ressource
    )
    data = returnOrderedDict(requete)
    try:
        resourceId = data["resources"]["resource"]["@id"]
    except:
        print("l'id de la ressource \"%s\" n'a pas été trouvé" % ressource)
        resourceId = None
    if resourceId is not None:

        ## Récupération des events
        requete = (
            url
            + "sessionId="
            + sessionId
            + "&function=getEvents&resources="
            + resourceId
            + "&startDate="
            + dateDebut
            + "&endDate="
            + dateFin
            + "&detail=8"
        )
        data = returnOrderedDict(requete)
        listeEvenements = list()
        try:
            event = data["events"]["event"]
        except:
            event = None
        if event is not None:
            if type(event) is not list:
                listeEvenements.append(event)
            else:
                listeEvenements = event
            for e in listeEvenements:
                dictCours = dict()
                idCours = e["@id"]
                date = e["@date"]
                startHour = e["@startHour"]
                endHour = e["@endHour"]
                resource = e["resources"]["resource"]
                listeTraineeId = list()
                listeCategory5Id = list()
                listeInstructorId = list()
                for r in resource:
                    if r["@category"] == "trainee":
                        stagiaire = r["@name"]
                        # stagiaireId = r["@id"]
                        listeTraineeId.append(r["@id"])
                    if r["@category"] == "category5":
                        listeCategory5Id.append(r["@id"])
                    if r["@category"] == "instructor":
                        instructeur = r["@name"]
                        # instructeurId = r["@id"]
                        listeInstructorId.append(r["@id"])
                    if r["@category"] == "equipment":
                        equipement = r["@name"]
                        equipementId = r["@id"]
                    if r["@category"] == "classroom":
                        salleDeCours = r["@name"]
                        salleDeCoursId = r["@id"]
                # print(idCours)
                # print(date)
                # print(startHour)
                # print(endHour)
                # print(stagiaire)
                # print(instructeur)
                # print(equipement)
                # print(salleDeCours)
                dictCours["idCours"] = idCours
                dictCours["date"] = date
                dictCours["startHour"] = startHour
                dictCours["endHour"] = endHour
                try:
                    dictCours["listeTraineeId"] = listeTraineeId
                except:
                    pass
                try:
                    dictCours["listeCategory5Id"] = listeCategory5Id
                except:
                    pass
                try:
                    dictCours["listeInstructorId"] = listeInstructorId
                except:
                    pass
                listeCours.append(dictCours)
    return listeCours
