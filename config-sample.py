## ********************************************************
## Variables à définir pour MySQL
hostBDD = ""
usernameBDD = ""
passwordBDD = ""
bdd = ""

## Variables à définir pour LDAP
serveurLDAP = ""
portLDAP = ""
usernameLDAP = "uid=exemple,ou=system,dc=exemple,dc=fr"
passwordLDAP = ""
baseLDAP = "dc=exemple,dc=fr"

## Variables à définir pour ADE
usernameADE = ""
passwordADE = ""
urlADE = "https://exemple.fr/jsp/webapi?"
anneeEnCours = ""
## ********************************************************

## Variable connexion AD Campus
hostnameAD = ""
usernameAD = "CN=exemple,DC=exemple,DC=fr"
passwordAD = ""
racineAD = "DC=exemple,DC=exemple,DC=fr"

racineGroupeAD = ""

# Variable Api Horizon

urlHVCS = "https://exemple.fr"
usernameHVCS = ""
domain = ""
pw = ""


# Variable Api AppVolume

urlServeurAppVolumes = "https://exemple.fr"
prefixApi = urlServeurAppVolumes + "/cv_api"
prefixAV = urlServeurAppVolumes + "/app_volumes"
loginAppV = ""
paswwordAppV = ""
domainAppV = ""

# Chemin ARRHES
pathARRHES = ""
